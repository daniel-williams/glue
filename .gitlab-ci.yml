build:sdist:
  stage: build
  image: ligo/lalsuite-dev:stretch
  script:
    - python setup.py sdist -d .
  artifacts:
    paths:
      - '*.tar.gz'

build:wheel:manylinux1:
  stage: build
  image: quay.io/pypa/manylinux1_x86_64
  script:
    - set -ex;
      for PYTHON in /opt/python/*/bin/python; do
        $PYTHON setup.py bdist_wheel;
      done
    - set -ex;
      for WHEEL in dist/*; do
        auditwheel repair -w . ${WHEEL};
      done
  artifacts:
    paths:
      - '*.whl'

build:wheel:macos:
  stage: build
  tags:
    - macos
  script:
    - set -ex;
      for VIRTUALENV in /opt/local/bin/virtualenv-?.?; do
        rm -rf env;
        $VIRTUALENV --system-site-packages env;
        source env/bin/activate;
        python setup.py bdist_wheel;
      done
    - pip install delocate
    - delocate-wheel -w . dist/*
  artifacts:
    paths:
      - '*.whl'

test:stretch:
  stage: test
  image: ligo/lalsuite-dev:stretch
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update
    - apt-get install -y lal-python
  script:
    - tar xf *.tar.*
    - cd $(find . -type d -maxdepth 1 -name 'lscsoft-glue-*')
    - python setup.py install
    - make -C test

test:el7:
  stage: test
  image: ligo/lalsuite-dev:el7
  variables:
    GIT_STRATEGY: none
  before_script:
    - yum install -y lal-python
  script:
    - tar xf *.tar.*
    - cd $(find . -type d -maxdepth 1 -name 'lscsoft-glue-*')
    - python setup.py install
    - make -C test

test:stretch:python3:
  stage: test
  image: ligo/lalsuite-dev:stretch
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update
    - apt-get install -y lal-python3
  script:
    - tar xf *.tar.*
    - cd $(find . -type d -maxdepth 1 -name 'lscsoft-glue-*')
    - python3 setup.py install
    - make -C test PYTHON=python3

deploy:stretch:
  stage: deploy
  image: ligo/lalsuite-dev:stretch
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update
    - apt-get -y install lintian debhelper python-all-dev python3-all-dev
  script:
    - tar xf *.tar.*
    - ln -s *.tar.* $(echo *.tar.* | sed 's/\(.*\)-\(.*\).\(tar.*\)/\1_\2.orig.\3/')
    - cd $(find . -type d -maxdepth 1 -name 'lscsoft-glue-*')
    - dpkg-buildpackage -us -uc
    - lintian --pedantic ../*.{deb,changes}
  artifacts:
    paths:
      - '*.deb'
      - '*.changes'

deploy:el7:
  stage: deploy
  image: ligo/base:el7
  variables:
    GIT_STRATEGY: none
  before_script:
    - yum install -y gcc make rpm-build rpmlint yum-utils
  script:
    - mkdir -p ~/rpmbuild/{SOURCES,SPECS}
    - tar -C ~/rpmbuild/SPECS -xf *.tar.* --strip-components 2 '*/etc/glue.spec'
    - mv *.tar.* ~/rpmbuild/SOURCES
    - yum-builddep -y ~/rpmbuild/SPECS/*.spec
    - rpmbuild -ba ~/rpmbuild/SPECS/*.spec
    - mv $(find ~/rpmbuild/{RPMS,SPECS,SRPMS} -type f) .
    - rpmlint *.{rpm,spec}
  artifacts:
    paths:
      - '*.rpm'
      - '*.spec'
